const express = require('express');
const Movie = require('../models/movie');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Movie.find().populate("_actors").then(objs => res.status(200).json({
    message: "Lista de peliculas del sistema",
    obj: objs
  })).catch(ex => res.status(500).json({
    message: "No se pudo consultar la informacion de las peliculas",
    obj: ex
  }));
}

function index(req, res, next) {
  const id = req.params.id;
  Movie.findOne({"_id":id}).populate("_actors").then(obj => res.status(200).json({
    message: `Pelicula almacenanda con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo consultar la informacion de la pelicula con ID: ${id}`,
    obj: ex
  }));
}

function create(req, res, next){
  const {
    genre,
    title,
    name,
    lastName,
    actors
  } = req.body;

  const director = new Object({
    _name: name,
    _lastName: lastName
  });

  let movie = new Movie({
    genre: genre,
    title: title,
    actors: actors,
    director: director
  });

  movie.save().then(obj => res.status(200).json({
    message: "Pelicula creada correctamente.",
    obj: obj
  })).catch(ex => res.status(500).json({
    message: "No se pudo almacenar la pelicula.",
    obj: ex
  }));

}

function replace(req, res, next) {
  const id = req.params.id;
  const genre = req.body.genre ? req.body.genre : "";
  const title = req.body.title ? req.body.title : "";
  const name = req.body.name ? req.body.name : "";
  const lastName = req.body.lastName ? req.body.lastName : object.lastName;
  const actors = req.body.actors ? req.body.actors : "";

  const director = new Object({
    _name: name,
    _lastName: lastName
  });

  let movie = new Object({
    _genre: genre,
    _title: title,
    _director: director,
    _actors: actors
  });

  Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
    message: `Pelicula reemplazada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo reemplazar la informacion de la pelicula con ID: ${id}`,
    obj: ex
  }));
}

async function edit(req, res, next) {
  const id = req.params.id;
  const {
    genre,
    title,
    name,
    lastName,
    actors
  } = req.body;

  const movie = await Movie.findOne({"_id":id});

  if(genre) {
    movie._genre = genre;
  }
  if(title) {
    movie._title = title;
  }
  if(name) {
    movie._name = name;
  }
  if(lastName) {
    movie._lastName = lastName;
  }
  if(actors) {
    movie._actors = actors;
  }

  movie.save().then(obj => res.status(200).json({
    message: `Pelicula editada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo editar la informacion de la pelicula con ID: ${id}`,
    obj: ex
  }));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Movie.remove({"_id":id}).then(obj => res.status(200).json({
    message: `Pelicula eliminada con ID: ${id}`,
    obj: obj
  })).catch(ex => res.status(500).json({
    message: `No se pudo eliminar la pelicula con ID: ${id}`,
    obj: ex
  }));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
