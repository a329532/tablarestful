const { json } = require('express');
const express = require('express');
const booking = require('../models/booking');
const Booking = require('../models/booking');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Booking.find().populate(_copy).populate(_member).then(objs => res.status(200).json({
        message: "Lista de reservaciones del sistema",
        obj: objs
    })).catch(ex => res.status(500).json({
        message: "No se puso consultar la informacion de las reservaciones",
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    Booking.findOne({"_id":id}).populate(_copy).populate(_member).then(obj => res.status(200).json({
        message: `Reservacion almacenada con ID ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        message: `No se encontro la reservacion almacenado con ID ${id}`,
        obj: ex
    }));
}

function create(req, res, next){
    const copy= req.body.copy;
    const member= req.body.member;
    const date= req.body.date;

    let booking = new Booking({
        copy:copy,
        member:member,
        date:date
    });

    booking.save().then(obj => res.status(200).json({
        message: 'Reservacion creada correctamente',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar la reservacion',
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const copy= req.body.copy ? req.body.name: "";
    const member= req.body.member ? req.body.member: "";
    const date= req.body.date ? req.body.date: "";

    let booking = new Object({
        _copy:copy,
        _member:member,
        _date:date
    });

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        message:"Reservacion remplazada correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo remplazar la reservacion',
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const copy= req.body.copy;
    const member= req.body.member;
    const date= req.body.date;

    let booking = new Object();

    if(copy){
        booking._copy = copy;
    }

    if(member){
        booking._member = member;
    }

    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id},booking).then(obj => res.status(200).json({
        message:"Reservacion actialuzada correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo actualizar la reservacion',
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        message:"Reservacion eliminada correctamente",
        obj:obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar la reservacion',
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
