const mongoose = require('mongoose');
//const Movie = mongoose.model('Movie');

const schema = mongoose.Schema({
    _format:String,
    //_movie:String,
    _movie:{type: mongoose.Schema.ObjectId, ref: "Movie"}, //dbreaf
    _number:Number,//integer
    _status:String
});

class Copy {
    
    constructor(format, movie, number, status){
        this._format=format;
        this._movie=movie;
        this._number=number;
        this._status=status;
    }

    //format
    get format(){
        return this._format;
    }
    set format(v){
        this._format=v;
    }

    //movie
    get movie(){
        return this._movie;
    }
    set movie(v){
        this._movie=v;
    }

    //number
    get number(){
        return this._number;
    }
    set number(v){
        this._number=v;
    }

    //status
    get status(){
        return this._status;
    }
    set status(v){
        this._status=v;
    }

}

schema.loadClass(Copy);
module.exports = mongoose.model('Copy', schema);
