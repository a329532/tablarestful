const mongoose = require('mongoose');
const copy = require('./copy');

const schema = mongoose.Schema({
    _member:{type: mongoose.Schema.ObjectId, ref: "Member"},
    _copy:{type: mongoose.Schema.ObjectId, ref: "Copy"},
    _date:Date
});

class Booking {
    
    constructor(copy, member, date){
        this._copy=copy;
        this._member=member;
        this._date=date;
    }

    //copy
    get copy(){
        return this._copy;
    }
    set copy(v){
        this._copy=v;
    }

    //member
    get member(){
        return this._member;
    }
    set member(v){
        this._member=v;
    }

    //date
    get date(){
        return this._date;
    }
    set date(v){
        this._date=v;
    }
}

schema.loadClass(Booking);
module.exports = mongoose.model('Booking', schema);
