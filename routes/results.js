const express = require('express');
const router = express.Router();

const controller = require('../controllers/results');

/* GET users listing. */
router.get('/:n/:n1', controller.sumar);
router.post('/', controller.multiplicar);
router.put('/', controller.dividir);
router.patch('/', controller.potencia);
router.delete('/:n/:n1', controller.restar);


module.exports = router;
